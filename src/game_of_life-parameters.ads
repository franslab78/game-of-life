--  Copyright (C) 2021  Frans Labuschagne
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <https://www.gnu.org/licenses/>.

private with Ada.Strings.Unbounded;
private with Ada.Containers.Vectors;

--  @summary
--  Game parameters.
--
--  @description
--  The parameters that controls the game.
package Game_Of_Life.Parameters is

   type Options is private;

   procedure Put;
   --  Print the options to standard output.

   function Get return Options;
   --  Get the options from standard input.

   function From_Command_Line (The_Options : out Options) return Boolean;
   --  Parse the game options from the command line.
   --  @param The_Options The options as parsed from the command line.
   --  @return True to continue with game, otherwise False to exit.

   function Get_Width (The_Options : in Options) return Dimension;
   --  Get the width of the world.
   --  @param The_Options The options.
   --  @return The width of the world.

   function Get_Height (The_Options : in Options) return Dimension;
   --  Get the height of the world.
   --  @param The_Options The options.
   --  @return The height of the world.

   function Get_Iterations (The_Options : in Options) return Positive;
   --  Get the amount of iterations to evolve the game.
   --  @param The_Options The options.
   --  @return The amount of iterations.

   function Amount_Of_Patterns (The_Options : in Options) return Natural;
   --  Get the amount of patterns.
   --  @param The_Options The options.
   --  @return The amount of patterns.

   function Get_Pattern_Row (The_Options : in Options;
                             Index       : in Positive) return Dimension;
   --  Get the row to place the pattern in the environment.
   --  @param The_Options The options.
   --  @param Index The index of the pattern.
   --  @return The row to place the pattern in the environment.

   function Get_Pattern_Column (The_Options : in Options;
                                Index       : in Positive) return Dimension;
   --  Get the column to place the pattern in the environment.
   --  @param The_Options The options.
   --  @param Index The index of the pattern.
   --  @return The column to place the pattern in the environment.

   function Get_Pattern_Path (The_Options : in Options;
                              Index       : in Positive) return String;
   --  Get the path of the pattern file.
   --  @param The_Options The options.
   --  @param Index The index of the pattern.
   --  @return The path to the pattern file.

private

   use Ada.Strings.Unbounded;

   type Pattern_Option is record
      Row    : Dimension;
      Column : Dimension;
      Path   : Unbounded_String;
   end record;

   package Patterns_Vectors is new Ada.Containers.Vectors
     (Index_Type => Positive, Element_Type => Pattern_Option);
   use Patterns_Vectors;

   subtype Patterns_Container is Vector;

   type Options is record
      Width      : Dimension;
      Height     : Dimension;
      Iterations : Positive;
      Patterns   : Patterns_Container;
   end record;

   function Get_Width (The_Options : in Options) return Dimension is
     (The_Options.Width);

   function Get_Height (The_Options : in Options) return Dimension is
      (The_Options.Height);

   function Get_Iterations (The_Options : in Options) return Positive is
     (The_Options.Iterations);

   function Amount_Of_Patterns (The_Options : in Options) return Natural is
     (Natural (The_Options.Patterns.Length));

   function Get_Pattern_Row (The_Options : in Options;
                             Index       : in Positive) return Dimension is
     (The_Options.Patterns (Index).Row);

   function Get_Pattern_Column (The_Options : in Options;
                                Index       : in Positive) return Dimension is
     (The_Options.Patterns (Index).Column);

   function Get_Pattern_Path (The_Options : in Options;
                              Index       : in Positive) return String is
     (To_String (The_Options.Patterns (Index).Path));


end Game_Of_Life.Parameters;

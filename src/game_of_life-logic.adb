--  Copyright (C) 2021  Frans Labuschagne
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <https://www.gnu.org/licenses/>.

with Ada.Text_IO; use Ada.Text_IO;

package body Game_Of_Life.Logic  is

   procedure Clear (A_World : in out World) is
   begin
      for Row in A_World'Range (1) loop
         for Col in A_World'Range (2) loop
            A_World (Row, Col) := 0;
         end loop;
      end loop;
   end Clear;


   procedure Next (A_World : in out World) is

      function Count_Moore_Neighbors (A_Row : Dimension; A_Col : Dimension)
                                      return Natural;
      --  https://en.wikipedia.org/wiki/Moore_neighborhood

      Above   : array (A_World'Range (2)) of Cell := (others => 0);
      Current : Cell                              := 0;
      Left    : Cell                              := 0;

      function Count_Moore_Neighbors (A_Row : Dimension; A_Col : Dimension)
                                      return Natural is
      begin
         return Above (A_Col - 1) + Above (A_Col) + Above (A_Col + 1) + Left +
           A_World (A_Row, A_Col + 1) + A_World (A_Row + 1, A_Col - 1) +
           A_World (A_Row + 1, A_Col) + A_World (A_Row + 1, A_Col + 1);
      end Count_Moore_Neighbors;

   begin
      for Row in A_World'First (1) + 1 .. A_World'Last (1) - 1 loop
         for Col in A_World'First (2) + 1 .. A_World'Last (2) - 1 loop
            case Count_Moore_Neighbors (Row, Col) is
               when 2      => Current := A_World (Row, Col);
               when 3      => Current := 1;
               when others => Current := 0;
            end case;
            Above (Col - 1)    := Left;
            Left               := A_World (Row, Col);
            A_World (Row, Col) := Current;
         end loop;
         Above (Above'Last - 1) := Left;
      end loop;
   end Next;


   procedure Add_Pattern (A_World   : in out World;
                          A_Row     : in     Dimension;
                          A_Col     : in     Dimension;
                          A_Pattern : in     Pattern) is

      World_Width    : constant Dimension :=
        A_World'Last (2) - A_World'First (2);
      World_Height   : constant Dimension :=
        A_World'Last (1) - A_World'First (1);
      Pattern_Width  : constant Dimension :=
        A_Pattern'Last (2) - A_Pattern'First (2);
      Pattern_Height : constant Dimension :=
        A_Pattern'Last (1) - A_Pattern'First (1);

   begin

      if (A_Row < (A_World'First (1) + 1)) or
        (A_Col < (A_World'First (2) + 1)) or
        ((A_Row + Pattern_Height - 1) > World_Height) or
        ((A_Col + Pattern_Width - 1) > World_Width)
      then
         --  The pattern does not fit on the board.
         raise Constraint_Error;
      else
         for Row in A_Pattern'First (1) .. A_Pattern'Last (1) loop
            for Col in A_Pattern'First (2) .. A_Pattern'Last (2) loop
               A_World (A_Row + Row, A_Col + Col) := A_Pattern (Row, Col);
            end loop;
         end loop;
      end if;

   end Add_Pattern;


   function Pattern_From_File (A_Path : in String) return Pattern is

      Input_File : File_Type;
      Rows       : Dimension := 1;
      Cols       : Dimension;

      procedure Get_Pattern_Dimension;
      function Get_Pattern_Details return Pattern;

      procedure Get_Pattern_Dimension is
      begin
         Open (File => Input_File,
               Mode => In_File,
               Name => A_Path);
         Cols := Dimension'Last;
         loop
            exit when End_Of_File (Input_File);
            declare
               Line : constant String := Get_Line (Input_File);
            begin
               if Line'Length < Cols then
                  Cols := Line'Length;
               end if;
            end;
            Rows := Rows + 1;
         end loop;
         Rows := Rows - 1;
         Close (Input_File);
      end Get_Pattern_Dimension;

      function Get_Pattern_Details return Pattern is
         The_Pattern : Pattern (1 .. Rows, 1 .. Cols) :=
           (others => (others => 0));
         Cell : Character;
      begin
         Open (File => Input_File,
               Mode => In_File,
               Name => A_Path);
         for Row in 1 .. Rows loop
            Set_Line (Input_File, Positive_Count (Row));
            Set_Col (Input_File, 1);
            for Col in 1 .. Cols loop
               Get (Input_File, Cell);
               if Cell /= ' ' and Cell /= '.' then
                  The_Pattern (Row, Col) := 1;
               end if;
            end loop;
         end loop;
         Close (Input_File);
         return The_Pattern;
      end Get_Pattern_Details;

   begin
      Get_Pattern_Dimension;
      return Get_Pattern_Details;
   end Pattern_From_File;

end Game_Of_Life.Logic;

with Game_Of_Life;            use Game_Of_Life;
with Game_Of_Life.Parameters; use Game_Of_Life.Parameters;
with Game_Of_Life.Logic;      use Game_Of_Life.Logic;
with Game_Of_Life.Renderer;   use Game_Of_Life.Renderer;

procedure Gameoflife is

   The_Options : Options;
   Should_Continue : constant Boolean := From_Command_Line (The_Options);

   The_World   : World (1 .. Get_Height (The_Options),
                        1 .. Get_Height (The_Options));

begin
   if Should_Continue then
      Clear (The_World);

      for I in 1 .. Amount_Of_Patterns (The_Options) loop
         declare
            The_Pattern : constant Pattern :=
              Pattern_From_File (Get_Pattern_Path (The_Options, I));
         begin
            Add_Pattern (The_World,
                         Get_Pattern_Row (The_Options, I),
                         Get_Pattern_Column (The_Options, I),
                         The_Pattern);
         end;
      end loop;

      Initialise;
      for I in 1 .. Get_Iterations (The_Options) loop
         delay (0.15);
         Render (The_World);
         Next (The_World);
      end loop;
      Uninitialise;
   end if;

end Gameoflife;

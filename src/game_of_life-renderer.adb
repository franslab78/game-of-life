--  Copyright (C) 2021  Frans Labuschagne
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <https://www.gnu.org/licenses/>.

with Ada.Text_IO; use Ada.Text_IO;
with ANSI;

package body Game_Of_Life.Renderer is

   procedure Initialise is
   begin
      Put (ANSI.Reset_All);
      Put (ANSI.Hide);
   end Initialise;


   procedure Uninitialise is
   begin
      New_Line (2);
      Put (ANSI.Show);
   end Uninitialise;


   procedure Render (A_World : in World) is
   begin

      for Row in A_World'First (1) .. A_World'Last (1) loop
         Put (ANSI.Previous);
      end loop;

      for Row in A_World'First (1) .. A_World'Last (1) loop
         Put (ANSI.Clear_Line);
         if Row = A_World'First (1) or Row = A_World'Last (1) then
            for Col in A_World'First (2) .. A_World'Last (2) loop
               Put ('#');
            end loop;
         else
            for Col in A_World'First (2) .. A_World'Last (2) loop
               if Col = A_World'First (2) or Col = A_World'Last (2) then
                  Put ('#');
               else
                  if Is_Alive (A_World (Row, Col)) = True then
                     Put ('O');
                  else
                     Put ('.');
                  end if;
               end if;
            end loop;
         end if;
         New_Line;
      end loop;
   end Render;

end Game_Of_Life.Renderer;

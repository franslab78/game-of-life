--  Copyright (C) 2021  Frans Labuschagne
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <https://www.gnu.org/licenses/>.

with Ada.Text_IO; use Ada.Text_IO;
with Ada.Command_Line; use Ada.Command_Line;

package body Game_Of_Life.Parameters is

   Invalid_Command_Line_Argument : exception;

   Default_Width      : constant := 40;
   Default_Height     : constant := 40;
   Default_Iterations : constant := 10;

   procedure Put is
   begin
      Put_Line ("Usage:");
      Put_Line ("   gameoflife [--width=<n>] [--height=<n>] [--iter=<n>] " &
                  "[--pattern=<r>,<c>,<file>]");
      Put_Line ("   gameoflife [-h] [--version]");
      New_Line;
      Put_Line ("Conway's Game of life.");
      New_Line;
      Put_Line ("   -h, --help                 display help and exit");
      Put_Line ("   -v, --version              display version information " &
                  "and exit");
      Put_Line ("   --width=<n>                width of the world");
      Put_Line ("   --height=<n>               height of the world");
      Put_Line ("   --iter=<n>                 amount of iterations");
      Put_Line ("   --pattern=<r>,<c>,<file>   add pattern from file to " &
                  "world at row and column");
      New_Line;
   end Put;


   function Get return Options is
      The_Options : Options;
   begin
      The_Options.Width      := Default_Width;
      The_Options.Height     := Default_Height;
      The_Options.Iterations := Default_Iterations;
      return The_Options;
   end Get;


   function From_Command_Line (The_Options : out Options) return Boolean is
      Should_Continue : Boolean := True;

      function Starts_With (A_Argument : in String; A_Value : in String)
                           return Boolean;
      function Starts_With (A_Argument : in String; A_Value : in String)
                           return Boolean is

      begin
         if A_Value'Length > A_Argument'Length then
            return False;
         else
            declare
               Partial : constant String := A_Argument
                 (A_Argument'First .. A_Argument'First + A_Value'Length - 1);
            begin
               return Partial = A_Value;
            end;
         end if;
      end Starts_With;

   begin
      The_Options.Width      := Default_Width;
      The_Options.Height     := Default_Height;
      The_Options.Iterations := Default_Iterations;
      if Argument_Count = 0 then
         The_Options := Get;
      else
         for I in 1 .. Argument_Count loop
            declare
               A_Argument : constant String := Argument (I);
            begin
               if A_Argument = "-h" or A_Argument = "--help" then
                  Put;
                  Should_Continue := False;
               elsif A_Argument = "-v" then
                  Put_Line (Short_Version);
                  Should_Continue := False;
               elsif A_Argument = "--version" then
                  Put (Long_Version);
                  New_Line;
                  Should_Continue := False;
               else
                  if Starts_With (A_Argument, "--iter=") then
                     The_Options.Iterations := Dimension'Value
                       (A_Argument (A_Argument'First + 7 .. A_Argument'Last));
                  elsif Starts_With (A_Argument, "--width=") then
                     The_Options.Width := Dimension'Value
                       (A_Argument (A_Argument'First + 8 .. A_Argument'Last));
                  elsif Starts_With (A_Argument, "--height=") then
                     The_Options.Height := Dimension'Value
                       (A_Argument (A_Argument'First + 9 .. A_Argument'Last));
                  elsif Starts_With (A_Argument, "--pattern=") then
                     declare
                        Pos : Positive  := A_Argument'First + 10;
                        Opt : Pattern_Option :=
                          (Row => 1, Column => 1,
                           Path => To_Unbounded_String (""));
                     begin
                        for I in Pos .. A_Argument'Last loop
                           if A_Argument (I) = ',' then
                              declare
                                 Row_String : constant String :=
                                   A_Argument (Pos .. (I - 1));
                              begin
                                 Opt.Row := Dimension'Value (Row_String);
                              end;
                              Pos := I + 1;
                              exit;
                           end if;
                        end loop;

                        if Pos = A_Argument'Last then
                           raise Invalid_Command_Line_Argument;
                        end if;

                        for I in Pos .. A_Argument'Last loop
                           if A_Argument (I) = ',' then
                              declare
                                 Col_String : constant String :=
                                   A_Argument (Pos .. (I - 1));
                              begin
                                 Opt.Column := Dimension'Value (Col_String);
                              end;
                              Pos := I + 1;
                              exit;
                           end if;
                        end loop;

                        if Pos = A_Argument'Last then
                           raise Invalid_Command_Line_Argument;
                        end if;

                        Opt.Path := To_Unbounded_String
                          (A_Argument (Pos .. A_Argument'Last));

                        The_Options.Patterns.Append (Opt);
                     end;
                  else
                     raise Invalid_Command_Line_Argument;
                  end if;
               end if;
            end;
         end loop;
      end if;

      return Should_Continue;
   end From_Command_Line;

end Game_Of_Life.Parameters;

--  Copyright (C) 2021  Frans Labuschagne
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <https://www.gnu.org/licenses/>.


--  @summary
--  Conway's game of life.
--
--  @description
--  This package contains Conway's Game of life.
--
--  See https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life for description.
--  See https://www.conwaylife.com/ref/lexicon/lex.htm for patterns.
package Game_Of_Life is

   Short_Version : constant String := "0.0.1";
   Long_Version  : constant String :=
     "Conway's Game of life.\n" &
     "Version:" & Short_Version &
     "Copyright (C) 2021  Frans Labuschagne\n" &
     "This program comes with ABSOLUTELY NO WARRANTY";

   subtype Dimension is Positive;
   --  Measure in one direction.

   subtype Cell is Natural range 0 .. 1;
   --  State of a cell: 0 dead, 1 alive.

   type World is array (Dimension range <>, Dimension range <>) of Cell;
   --  The environment of cells where the game evolves in.

   type Pattern is array (Dimension range <>, Dimension range <>) of Cell;
   --  A pattern od cells that can be placed inside the environment.


   function Is_Alive (Which : in Cell) return Boolean;
   --  Query if the cell is alive.
   --  @param Which The cell to query.
   --  @return True if the cell is alive, otherwise false.


   function Is_Dead (Which : in Cell) return Boolean;
   --  Query if the cell is dead.
   --  @param Which The cell to query.
   --  @return True if the cell is dead, otherwise false.


   --  Predefined patterns to add are:
   Blinker : constant Pattern := ((0, 0, 0), (1, 1, 1), (0, 0, 0));

   Toad : constant Pattern := ((0, 1, 1, 1),
                               (1, 1, 1, 0));

   Beacon : constant Pattern := ((1, 1, 0, 0),
                                 (1, 0, 0, 0),
                                 (0, 0, 0, 1),
                                 (0, 0, 1, 1));

   Glider : constant Pattern := ((1, 1, 1),
                                 (1, 0, 0),
                                 (0, 1, 0));

end Game_Of_Life;

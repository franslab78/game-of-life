--  Copyright (C) 2021  Frans Labuschagne
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <https://www.gnu.org/licenses/>.

--  @summary
--  The logic of the game.
--
--  @description
--  The logic of the game.
package Game_Of_Life.Logic is

   procedure Clear (A_World : in out World);
   --  Clear the worlds of any patterns.
   --  @param A_World The world to clear.


   procedure Next (A_World : in out World);
   --  Apply Conway's game of life rules to a world to determine the
   --  next iteration.
   --  @param A_World The world.


   procedure Add_Pattern (A_World   : in out World;
                          A_Row     : in     Dimension;
                          A_Col     : in     Dimension;
                          A_Pattern : in     Pattern);
   --  Add a pettern to the world at the specified location.
   --  Do not place a patern on the boarder of the board.
   --  @param A_World The world.
   --  @param A_Row The row on the playing board to add the pattern.
   --  @param A_Col The column on the playing board to add the pattern.
   --  @param A_Pattern The pattern to add to the playing board.


   function Pattern_From_File (A_Path : in String) return Pattern;
   --  Load a pattern from a file.
   --  @param A_Path The path to the pattern file.
   --  @return The pattern read from the file.

end Game_Of_Life.Logic;

--  Copyright (C) 2021  Frans Labuschagne
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <https://www.gnu.org/licenses/>.


package body Game_Of_Life is

   function Is_Alive (Which : in Cell) return Boolean is
   begin
      if Which = 1 then
         return True;
      else
         return False;
      end if;
   end Is_Alive;


   function Is_Dead (Which : in Cell) return Boolean is
   begin
      if Which = 0 then
         return True;
      else
         return False;
      end if;
   end Is_Dead;

end Game_Of_Life;

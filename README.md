# game-of-life

[Conway's Game Of Life](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life)
written in Ada.

# Source Code

## Clone

**game-of-life** is hosted in a [GIT](https://git-scm.com/) repository at [https://gitlab.com/franslab78/game-of-life](https://gitlab.com/franslab78/game-of-life).
The source code can be cloned as follows:

### On Windows

```bash
md location\to\clone\into & cd location\to\clone\into
git clone https://gitlab.com/franslab78/game-of-life.git
```
### On Linux

```bash
mkdir location/to/clone/into; cd location/to/clone/into
git clone https://gitlab.com/franslab78/game-of-life.git
```

# Build

**game-of-life** makes use of the [Ada Library Repository](https://alire.ada.dev/).

```bash
cd location\to\clone\into\game-of-life
alr build
```

## Dependencies

The game-of-life depends on [Ansiada](https://alire.ada.dev/crates/ansiada) with will be downloaded when build by alr.